from django.apps import AppConfig


class RucodeConfig(AppConfig):
    name = 'rucode'
