# Generated by Django 2.2.12 on 2020-04-08 06:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rucode', '0004_auto_20200408_0620'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tipo_codigo',
            name='activo_desde',
            field=models.DateField(verbose_name='Activo desde'),
        ),
    ]
