from django.db import models
from django.core.exceptions import ValidationError
import datetime


class Tipo_codigo(models.Model):
	nombre = models.CharField('Tipo de codigo', max_length=200)
	activo_desde= models.DateField('Activo desde',null=False, blank=True)
	activo_hasta= models.DateField('Activo Hasta',null=True, blank=True)

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs ):
		self.nombre = self.nombre.upper()
		self.full_clean()
		return super(Tipo_codigo,self).save(*args,**kwargs)

	def clean(self):
		if self.activo_desde is None:
			raise ValidationError('La FECHA DESDE no puede quedar vacia')

		if self.activo_desde is not None:
			if self.activo_desde>datetime.date.today():
				raise ValidationError('La FECHA DESDE no puede ser mayor a la FECHA ACTUAL')				

		if self.activo_hasta is not None:
			if self.activo_desde > self.activo_hasta:
				raise ValidationError('La FECHA DESDE debe ser menor o igual que la FECHA HASTA')

	class Meta:
		verbose_name = 'Tipo de Codigo'
		verbose_name_plural = 'Tipos de codigo'

class Organismo(models.Model):
	nombre = models.CharField('Organismo', max_length=200)
	activo_desde= models.DateField('Activo desde',null=False, blank=True)
	activo_hasta= models.DateField('Activo Hasta',null=True, blank=True)

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwargs ):
		self.nombre = self.nombre.upper()
		self.full_clean()
		return super(Organismo,self).save(*args,**kwargs)

	def clean(self):
		if self.activo_desde is None:
			raise ValidationError('La FECHA DESDE no puede quedar vacia')

		if self.activo_desde is not None:
			if self.activo_desde>datetime.date.today():
				raise ValidationError('La FECHA DESDE no puede ser mayor a la FECHA ACTUAL')				

		if self.activo_hasta is not None:
			if self.activo_desde > self.activo_hasta:
				raise ValidationError('La FECHA DESDE debe ser menor o igual que la FECHA HASTA')



	class Meta:
		verbose_name = 'Organismo'
		verbose_name_plural = 'Organismo'

class Tipo_documento(models.Model):
	nombre = models.CharField('Tipo de Documento', max_length=200)
	codigo = models.CharField('Codigo', max_length=3, unique=True)
	activo_desde= models.DateField('Activo desde',null=False, blank=True)
	activo_hasta= models.DateField('Activo Hasta',null=True, blank=True)

	def __str__(self):
		return self.nombre.upper()

	def save(self, *args, **kwargs ):
		self.nombre = self.nombre.upper()
		self.codigo = self.codigo.upper()
		self.full_clean()
		return super(Tipo_documento,self).save(*args,**kwargs)

	def clean(self):
		if self.activo_desde is None:
			raise ValidationError('La FECHA DESDE no puede quedar vacia')

		if self.activo_desde is not None:
			if self.activo_desde>datetime.date.today():
				raise ValidationError('La FECHA DESDE no puede ser mayor a la FECHA ACTUAL')				

		if self.activo_hasta is not None:
			if self.activo_desde > self.activo_hasta:
				raise ValidationError('La FECHA DESDE debe ser menor o igual que la FECHA HASTA')

	class Meta:
		verbose_name = 'Tipo de Documento'
		verbose_name_plural = 'Tipos de Documento'