from django.contrib import admin
from rucode.models import *


@admin.register(Tipo_codigo)
class Tipo_codigoAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'activo_desde', 'activo_hasta']
	list_filter = ['nombre']

@admin.register(Organismo)
class OrganismoAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'activo_desde', 'activo_hasta']
	list_filter = ['nombre']

@admin.register(Tipo_documento)
class Tipo_DocumentoAdmin(admin.ModelAdmin):
	list_display = ['codigo','nombre', 'activo_desde', 'activo_hasta']
	list_filter = ['nombre']